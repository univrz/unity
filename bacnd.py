import feedparser
import dbf


def parser(table,url,limit):
    feed = feedparser.parse(url)
    dbf.delete_tables(table)
    dbf.text_in(table)
    for i in range(0,limit):
        entry = feed.entries[i]
        z=str(entry.title)
        l=str(entry.link)
        dbf.insert__into_table(table,z,l)
    return ("insertion done")

def main():
    result= parser("ge","https://www.theguardian.com/uk/environment/rss",20)
    result= parser("gs","https://www.theguardian.com/science/rss",20)
    result= parser("gt","https://www.theguardian.com/technology/rss",20)
    result= parser("gw","https://www.theguardian.com/world/rss",20)
    



if __name__=='__main__':
    parser("eff","https://www.eff.org/rss/updates.xml")
    
