from flask import Flask, render_template, request, redirect, url_for
import  bacnd
import dbf
app = Flask(
    __name__,
    template_folder = 'template'
)

@app.route("/")
def index():
    bacnd.main()
    texts7=dbf.select_titles_fromtable("gw")
    urls7=dbf.select_link_fromtable("gw")
    texts1=dbf.select_titles_fromtable("gt")
    urls1=dbf.select_link_fromtable("gt")
    texts2=dbf.select_titles_fromtable("gs")
    urls2=dbf.select_link_fromtable("gs")
    texts3=dbf.select_titles_fromtable("ge")
    urls3=dbf.select_link_fromtable("ge")
    texts4=dbf.select_titles_fromtable("bw")
    urls4=dbf.select_link_fromtable("bw")
    texts5=dbf.select_titles_fromtable("bt")
    urls5=dbf.select_link_fromtable("bt")
    texts6=dbf.select_titles_fromtable("bs")
    urls6=dbf.select_link_fromtable("bs")
    return render_template('index.html',urls7=urls7,texts7=texts7,urls1=urls1,texts1=texts1,urls2=urls2,texts2=texts2,urls3=urls3,texts3=texts3,urls4=urls4,texts4=texts4,urls5=urls5,texts5=texts5,urls6=urls6,texts6=texts6)


if __name__ == '__main__':
    app.run(debug=True)
